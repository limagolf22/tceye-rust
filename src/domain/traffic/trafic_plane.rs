pub struct TraficPlane {
    pub cruise_speed: i32,
    pub glide_ratio: i32,
}
