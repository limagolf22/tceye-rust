#![allow(dead_code)]
#![allow(unused)]

use crate::domain::traffic::trafic_plane::TraficPlane;

pub struct ConflictParams {
    pub time_sep: i32,
    pub bearing: f32,
    pub vertical_sep: f64,
    pub traffic: TraficPlane,
}
