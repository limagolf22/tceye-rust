use std::f64::consts::PI;

use crate::domain::conflict::geo_computations::{sum_angles, GeoPoint};
use crate::domain::traffic::trafic_plane::TraficPlane;
use crate::domain::simconnectprox::simconnect_params::POS_struct;
use crate::domain::tools::consts::NM_2_FT;

pub fn create_crossing_trafic(
    target_position: &POS_struct,
    gap: u32,
    trafic: TraficPlane,
    bearing: f64,
) -> POS_struct {
    let mut crossing_point = get_point_from_struct(&target_position);
    crossing_point.displace_position_from(
        target_position.heading,
        (gap as f64) * target_position.speed / 3600.0,
        0,
    );
    crossing_point.displace_position_from(
        sum_angles(target_position.heading, bearing as f64),
        (gap as f64) * (trafic.cruise_speed as f64) / 3600.0,
        0,
    );
    let mut pos_res = get_struct_from_point(&crossing_point);

    pos_res.speed = trafic.cruise_speed as f64;

    pos_res.heading = sum_angles(target_position.heading, sum_angles(bearing, PI));
    pos_res.altitude += ((4 * gap) as f64 * target_position.speed / 3600.0 * NM_2_FT
        / (trafic.glide_ratio as f64)) as f32;
    pos_res
}

fn get_point_from_struct(pos_struct: &POS_struct) -> GeoPoint {
    GeoPoint {
        lon: pos_struct.longitude,
        lat: pos_struct.latitude,
        altitude: pos_struct.altitude as f64,
    }
}

fn get_struct_from_point(point: &GeoPoint) -> POS_struct {
    POS_struct {
        kohlsman: 29.92f32,
        altitude: point.altitude as f32,
        latitude: point.lat,
        longitude: point.lon,
        roll: 0.0,
        pitch: 0.0,
        heading: 0.0,
        speed: -1.0,
    }
}

#[cfg(test)]
mod tests {
    use std::f64::consts::PI;

    use crate::domain::conflict::conflict_generator::create_crossing_trafic;
    use crate::domain::traffic::trafic_plane::TraficPlane;
    use crate::domain::simconnectprox::simconnect_params::POS_struct;
    use crate::domain::tools::consts::DEG_2_RAD;

    #[test]
    fn create_crossing_trafic_test() {
        let target_struct = POS_struct {
            kohlsman: 29.92,
            altitude: 3000.0,
            latitude: (45.0 - 5.0 / 60.0) * DEG_2_RAD,
            longitude: 1.0 * DEG_2_RAD,
            roll: 0.0,
            pitch: 0.0,
            heading: 0.0,
            speed: 60.0,
        };

        let trafic = TraficPlane {
            cruise_speed: 60,
            glide_ratio: 48,
        };

        let pos_struct1 = create_crossing_trafic(&target_struct, 300, trafic, PI / 2.0);

        let expected_struct = POS_struct {
            altitude: 5531.715f32,
            heading: 270.0 * DEG_2_RAD,
            kohlsman: 29.92,
            latitude: (45.0 + 0.0 / 60.0) * DEG_2_RAD,
            longitude: (1.0 + 5.0 / 60.0 * 2.0 / (2.0f64).sqrt()) * DEG_2_RAD,
            pitch: 0.0,
            roll: 0.0,
            speed: 60.0,
        };
        compare_pos_structs(&expected_struct, &pos_struct1);
    }

    fn compare_pos_structs(expected: &POS_struct, value: &POS_struct) {
        assert_eq!(expected.altitude, value.altitude);
        assert_eq!(expected.heading, value.heading);
        assert_eq!(expected.latitude, value.latitude);
        assert_eq!(expected.longitude as f32, value.longitude as f32);
        assert_eq!(expected.speed, value.speed);
    }
}
