use crate::domain::tools::consts::DEG_2_RAD;
use std::f64::consts::PI;

pub struct GeoPoint {
    pub lon: f64,
    pub lat: f64,
    pub altitude: f64,
}

impl GeoPoint {
    pub fn displace_position_from(&mut self, bearing: f64, distance: f64, separation: i32) {
        self.add_steps(
            distance * bearing.sin(),
            distance * bearing.cos(),
            separation as f64,
        )
    }

    fn add_steps(&mut self, delta_x: f64, delta_y: f64, delta_z: f64) {
        self.lon += delta_x / self.lat.cos() / 60.0 * DEG_2_RAD;
        self.lat += delta_y / 60.0 * DEG_2_RAD;
        self.altitude += delta_z;
    }
}

pub fn sum_angles(angle1: f64, angle2: f64) -> f64 {
    let mut angle_res: f64 = angle1 + angle2;
    if angle_res > PI * 2.0 {
        angle_res -= PI * 2.0;
    } else if angle_res < 0.0 {
        angle_res += PI * 2.0;
    }
    angle_res
}

#[cfg(test)]
mod tests {
    use crate::domain::{conflict::geo_computations::GeoPoint, tools::consts::DEG_2_RAD};

    #[test]
    fn displace_position_from_test() {
        let expected_point1 = GeoPoint {
            lon: 0.0,
            lat: 31.0 * DEG_2_RAD,
            altitude: 2500.0,
        };

        let mut point = GeoPoint {
            lon: 0.0,
            lat: 30.0 * DEG_2_RAD,
            altitude: 2000.0,
        };

        point.displace_position_from(0.0, 60.0, 500.0 as i32);

        assert_eq!(expected_point1.lon, point.lon);
        assert_eq!(expected_point1.lat, point.lat);
        assert_eq!(expected_point1.altitude, point.altitude);
    }
}
