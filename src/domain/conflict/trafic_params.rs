use crate::domain::traffic::trafic_plane::TraficPlane;

pub struct TraficParams {
    pub time_sep: u32,
    pub bearing: f32,
    pub vertical_sep: f64,
    pub trafic: TraficPlane
}