use std::f64::consts::PI;

pub const DEG_2_RAD: f64 = PI / 180.0;
pub const NM_2_FT: f64 = 1852.0 / 0.3048;
