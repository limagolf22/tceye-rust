
pub trait ScenarioProcessor {
    fn start(&self);
    
    fn stop(&self);
}