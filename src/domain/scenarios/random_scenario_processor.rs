#![allow(dead_code)]
#![allow(unused)]

use crate::domain::scenarios::scenario_processor::ScenarioProcessor;
use crate::domain::simconnectprox::sim_proxy::SimProxy;
use log::info;

pub struct RandomScenarioProcessor {
    sim_proxy: Box<dyn SimProxy>,
}

impl RandomScenarioProcessor {
    fn new(simconnect_proxy_arg: Box<dyn SimProxy>) -> RandomScenarioProcessor {
        RandomScenarioProcessor {
            sim_proxy: simconnect_proxy_arg,
        }
    }
}

impl ScenarioProcessor for RandomScenarioProcessor {
    fn start(&self) {
        info!("Random Scenario Generator started");
    }

    fn stop(&self) {
        info!("Random Scenario Generator stopped");
    }
}

#[cfg(test)]
mod tests {
    use crate::domain::scenarios::random_scenario_processor::RandomScenarioProcessor;
    use crate::domain::scenarios::scenario_processor::ScenarioProcessor;
    use crate::domain::simconnectprox::sim_proxy::MockSimProxy;

    #[test]
    fn create_rdm_scenario_generator_test() {
        // env_logger::init();
        let mock_simconnect = MockSimProxy::new();

        let generator = RandomScenarioProcessor::new(Box::new(mock_simconnect));
        generator.start();
        generator.stop();
        assert_eq!(0, 0);
    }
}
