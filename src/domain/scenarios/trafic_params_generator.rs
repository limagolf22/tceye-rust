use log::info;
use rand::prelude::*;

use crate::domain::{conflict::trafic_params::TraficParams, tools::consts::DEG_2_RAD, traffic::trafic_plane::TraficPlane};

pub fn create_random_trafic_params()-> TraficParams {
    let rdm:f64 = rand::thread_rng().gen();

    let rdm_bearing = (rdm-0.5) * 200.0 * DEG_2_RAD;
    let rdm_speed = 60.0 + 10.0 * rdm;
    let rdm_impact_time = (10.0 + rdm * 25.0) as u32;

    let plane = TraficPlane {cruise_speed:(rdm_speed as i32), glide_ratio:48};
    let trafic = TraficParams {
        time_sep: rdm_impact_time,
        bearing: rdm_bearing as f32,
        vertical_sep: 0.0,
        trafic: plane
    };
    info!("Trafic created coming from the {}°, at {}kts, above {}ft in {}s", trafic.bearing / DEG_2_RAD as f32, trafic.trafic.cruise_speed, trafic.vertical_sep, trafic.time_sep);
    trafic
}

#[cfg(test)]
mod tests {

    use crate::domain::scenarios::trafic_params_generator::create_random_trafic_params;

    #[test]
    fn create_random_trafic_params_test() {
        let _trafic = create_random_trafic_params();
        assert_eq!(0, 0);
    }
}
