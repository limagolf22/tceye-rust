#![allow(non_camel_case_types)]

pub enum COMMAND {
    ADD_REQ,
    ADD_VAL_DEF,
    DEF_REQ,
    TP_REQ,
    WRITE_TEXT,
    START_RECORD,
    RESET_RECORD,
    START_REPLAY,
    SEND_DATABASE,
    INIT_RECORD,
    GET_USER_DATAS,
}

pub enum DEFINITIONS {
    Def_POS,
    Def_INIT_POS,
    DEF_WP,
}

pub enum REQUESTS {
    REQ_POS,
    REQ_CREATE_AI,
}

pub enum EVENTS {
    TXT_EVENT,
}

#[derive(Clone, Copy, Debug, Default)]
pub struct POS_struct {
    pub kohlsman: f32,
    pub altitude: f32,
    pub latitude: f64,
    pub longitude: f64,
    pub roll: f64,
    pub pitch: f64,
    pub heading: f64,
    pub speed: f64,
}

pub struct SIMCONNECT_DATA_INITPOSITION2 {
    pub latitude: f64,
    pub longitude: f64,
    pub altitude: f64,
    pub pitch: f64,
    pub bank: f64,
    pub heading: f64,
    pub on_ground: usize,
    pub airspeed: usize,
}
