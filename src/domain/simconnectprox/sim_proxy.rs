use crate::domain::simconnectprox::simconnect_params::POS_struct;
use mockall::predicate::*;
use mockall::*;

#[automock]
pub trait SimProxy {
    /// Retrieves the last known position of the player in the simulation.
    ///
    /// # Returns
    ///
    /// - `POS_struct` : A structure containing the player's coordinates.
    fn get_last_pos_of_player(&self) -> POS_struct;

    /// Initializes the necessary simulation events for the proxy.
    ///
    /// This function sets up the simulation by configuring the events required for future interactions.
    fn init_sim_events(&mut self);

    /// Reads and updates simulation values.
    ///
    /// This function gathers data from the simulation at each call
    /// and updates it within the proxy's internal context. It's typically
    /// called at regular intervals to keep the data up-to-date.
    fn read_sim_values(&mut self);

    /// Reads specific values from the simulation with an option for conditional updating.
    ///
    /// # Parameters
    ///
    /// - `true_update` : A boolean indicating whether a full update (`true`) or a partial update (`false`) should be performed.
    ///
    /// # Returns
    ///
    /// - `i32` : An integer representing a result code or a value read from the simulation.
    fn read_simple_values(&self, true_update: bool) -> i32;

    /// Creates an AI aircraft at the specified position.
    ///
    /// This function commands the simulation to create an AI-controlled aircraft at a given position.
    ///
    /// # Parameters
    ///
    /// - `aircraft_pos` : A `POS_struct` structure specifying the position of the aircraft to be created.
    fn call_ai_aircraft_creation(&mut self, aircraft_pos: POS_struct);

    /// Closes the connection with the simulation.
    ///
    /// This function is used to properly terminate the connection to the simulator,
    /// performing any necessary cleanup actions.
    fn quit_connexion(&self);

    /// Adds waypoints to a specific object in the simulation.
    ///
    /// This function allows you to add a series of waypoints to an object identified by its ID.
    ///
    /// # Parameters
    ///
    /// - `object_id` : A unique identifier (u32) for the object to which the waypoints should be added.
    fn add_waypoints_to_object(&self, object_id: u32);
}
