#![allow(dead_code)]
#![allow(unused)]

use crate::domain::simconnectprox::sim_proxy::SimProxy;
use crate::domain::simconnectprox::simconnect_params::POS_struct;
use crate::domain::simconnectprox::simconnect_params::DEFINITIONS::{
    Def_INIT_POS, Def_POS, DEF_WP,
};
use crate::domain::simconnectprox::simconnect_params::REQUESTS::{REQ_CREATE_AI, REQ_POS};
use crate::domain::tools::consts::DEG_2_RAD;
use log::{debug, info};
use simconnect::{
    self, SIMCONNECT_DATA_WAYPOINT,
    SIMCONNECT_SIMOBJECT_TYPE_SIMCONNECT_SIMOBJECT_TYPE_AIRCRAFT, SIMCONNECT_WAYPOINT_ON_GROUND,
};
use simconnect::{
    DispatchResult, SimConnector, DWORD, SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
    SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64, SIMCONNECT_DATA_INITPOSITION,
    SIMCONNECT_DATA_REQUEST_FLAG_DEFAULT, SIMCONNECT_DATA_REQUEST_ID, SIMCONNECT_OBJECT_ID_USER,
    SIMCONNECT_PERIOD_SIMCONNECT_PERIOD_VISUAL_FRAME,
};

// To allign the memory we have to set a fixed max size to the returned variables from the game
const MAX_RETURNED_ITEMS: usize = 255;

// Rust will add padding to the inner parts of a struct if it isn't marked as packed
// The way Simconnect returns values is unaligned data in C style
#[repr(C, packed)]
struct KeyValuePairFloat {
    id: DWORD,
    value: f64,
}
struct DataFloatStruct {
    data: [KeyValuePairFloat; MAX_RETURNED_ITEMS],
}
#[repr(C, packed)]
struct KeyValuePairString {
    id: DWORD,
    // Strings get returned as max 255 bytes
    value: [u8; 255],
}

struct DataStringStruct {
    data: [KeyValuePairString; MAX_RETURNED_ITEMS],
}

struct InnerPosStruct {
    kohlsman: f64,
    altitude: f64,
    latitude: f64,
    longitude: f64,
    roll: f64,
    pitch: f64,
    heading: f64,
    speed: f64,
}

struct DataStruct {
    lat: f64,
    lon: f64,
    alt: f64,
    speed: f64,
}

pub struct SimConnectProxy {
    last_pos: POS_struct,
    simconnector: SimConnector,
    trafic_init_position: SIMCONNECT_DATA_INITPOSITION,
}

unsafe impl Send for SimConnectProxy {}

unsafe impl Sync for SimConnectProxy {}

impl SimConnectProxy {
    pub fn new() -> SimConnectProxy {
        SimConnectProxy {
            last_pos: POS_struct {
                kohlsman: 0.0,
                altitude: 0.0,
                latitude: 0.0,
                longitude: 0.0,
                roll: 0.0,
                pitch: 0.0,
                heading: 0.0,
                speed: 0.0,
            },
            simconnector: SimConnector::new(),
            trafic_init_position: SIMCONNECT_DATA_INITPOSITION {
                Latitude: 0.0,
                Longitude: 0.0,
                Altitude: 0.0,
                Pitch: 0.0,
                Bank: 0.0,
                Heading: 0.0,
                OnGround: 0,
                Airspeed: 65,
            },
        }
    }
}

impl SimProxy for SimConnectProxy {
    fn get_last_pos_of_player(&self) -> POS_struct {
        self.last_pos
    }

    fn init_sim_events(&mut self) {
        let ret_connect = self.simconnector.connect("Simconnect Program");
        info!(
            "Connected To Microsoft Flight Simulator 2020 : {}",
            ret_connect
        );

        self.simconnector
            .subscribe_to_system_event(Def_INIT_POS as DWORD, "ObjectAdded");

        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "Kohlsman setting hg",
            "inHg",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "PLANE ALTITUDE",
            "feet",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "PLANE LATITUDE",
            "Radians",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "PLANE LONGITUDE",
            "Radians",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "Plane Bank Degrees",
            "Radians",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "Plane Pitch Degrees",
            "Radians",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "Plane Heading Degrees magnetic",
            "Radians",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.add_data_definition(
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            "AIRSPEED INDICATED",
            "knots",
            SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
            u32::MAX,
            0.0,
        );
        self.simconnector.request_data_on_sim_object(
            REQ_POS as SIMCONNECT_DATA_REQUEST_ID,
            Def_POS as SIMCONNECT_CLIENT_DATA_DEFINITION_ID,
            SIMCONNECT_OBJECT_ID_USER,
            SIMCONNECT_PERIOD_SIMCONNECT_PERIOD_VISUAL_FRAME,
            SIMCONNECT_DATA_REQUEST_FLAG_DEFAULT,
            0,
            0,
            0,
        );

    }

    fn read_simple_values(&self, true_update: bool) -> i32 {
        let mess = self.simconnector.get_next_message();
        let mut res = -1;

        match mess {
            Ok(simconnect::DispatchResult::SimObjectData(data)) => unsafe {
                if data.dwDefineID == 0 {
                    let sim_data_ptr = std::ptr::addr_of!(data.dwData) as *const DataStruct;
                    let sim_data_value = std::ptr::read_unaligned(sim_data_ptr);
                    if true || true_update {
                        // print!(
                        //     "\r{:?},{:?},{:?},{:?}",
                        //     sim_data_value.lat, sim_data_value.lon, sim_data_value.alt, sim_data_value.speed
                        // );
                        debug!("{:?}", sim_data_value.alt.round());
                    }
                    res = sim_data_value.alt as i32;
                }
            },
            _ => (),
        }

        res
    }

    fn read_sim_values(&mut self) {
        match self.simconnector.get_next_message() {
            Ok(DispatchResult::SimObjectData(data)) => unsafe {
                match data.dwDefineID {
                    // Here we match the define_id we've passed using the request_data_on_sim_object
                    0 => {
                        let sim_data_ptr = std::ptr::addr_of!(data.dwData) as *const InnerPosStruct;
                        let sim_data_value = std::ptr::read_unaligned(sim_data_ptr);
                        // The amount of floats received from the sim
                        if false {
                            debug!(
                                "{:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?}",
                                sim_data_value.kohlsman,
                                sim_data_value.altitude,
                                sim_data_value.latitude,
                                sim_data_value.longitude,
                                sim_data_value.pitch,
                                sim_data_value.roll,
                                sim_data_value.heading,
                                sim_data_value.speed
                            );
                        }

                        self.last_pos = POS_struct {
                            kohlsman: sim_data_value.kohlsman as f32,
                            altitude: sim_data_value.altitude as f32,
                            latitude: sim_data_value.latitude,
                            longitude: sim_data_value.longitude,
                            roll: sim_data_value.roll,
                            pitch: sim_data_value.pitch,
                            heading: sim_data_value.heading,
                            speed: sim_data_value.speed,
                        }
                    }
                    1 => {
                        let sim_data_ptr =
                            std::ptr::addr_of!(data.dwData) as *const DataStringStruct;
                        // The amount of strings received from the sim
                        let count = data.dwDefineCount as usize;
                        let sim_data_value = std::ptr::read_unaligned(sim_data_ptr);
                        for i in 0..count {
                            //since we only defined 1 string variable the key returned should be 4
                            let key = sim_data_value.data[0].id;
                            //byte array to string
                            let string =
                                std::str::from_utf8(&sim_data_value.data[i].value).unwrap();
                            info!("{}", key);
                            info!("{}", string);
                        }
                    }
                    _ => (),
                }
            },
            Ok(DispatchResult::EventObjectAddRemove(data)) => {
                debug!("object added or removed : {}",data._base.dwData as i32);
                if data.eObjType == SIMCONNECT_SIMOBJECT_TYPE_SIMCONNECT_SIMOBJECT_TYPE_AIRCRAFT {
                    self.add_waypoints_to_object(data._base.dwData);
                }
            }
            Ok(DispatchResult::Open(_)) => {
                info!("Connected to simulator.");
            }
            Ok(DispatchResult::Quit(_)) => {
                info!("Disconnected from simulator.");
            }
            _ => (),
        }
    }

    fn call_ai_aircraft_creation(&mut self, aircraft_pos: POS_struct) {
        debug!("call ai creation");
        let position = create_init_pos_from_pos_struct(aircraft_pos);
        
        self.trafic_init_position = position;
        
        self.simconnector.ai_create_non_atc_aircraft(
            "Asobo LS8 18",
            "22",
            position,
            REQ_CREATE_AI as DWORD,
        );
        
        fn create_init_pos_from_pos_struct(pos_struct: POS_struct) -> SIMCONNECT_DATA_INITPOSITION {
            SIMCONNECT_DATA_INITPOSITION {
                Latitude: pos_struct.latitude / DEG_2_RAD,
                Longitude: pos_struct.longitude / DEG_2_RAD,
                Altitude: pos_struct.altitude as f64,
                Pitch: 0.0,
                Bank: 0.0,
                Heading: pos_struct.heading / DEG_2_RAD,
                OnGround: 0,
                Airspeed: pos_struct.speed as DWORD,
            }
        }
    }

    fn add_waypoints_to_object(&self, object_id: u32) {
        let waypoints = [
            SIMCONNECT_DATA_WAYPOINT {
                Flags: SIMCONNECT_WAYPOINT_ON_GROUND,
                ktsSpeed: self.trafic_init_position.Airspeed as f64,
                Latitude: self.trafic_init_position.Latitude
                    + 0.01 * (self.trafic_init_position.Heading * DEG_2_RAD).cos(),
                Longitude: self.trafic_init_position.Longitude
                    + 0.01 * (self.trafic_init_position.Heading * DEG_2_RAD).sin()
                        / (self.trafic_init_position.Latitude * DEG_2_RAD).cos(),
                Altitude: self.trafic_init_position.Altitude * 0.0,
                percentThrottle: 100.0,
            },
            SIMCONNECT_DATA_WAYPOINT {
                Flags: SIMCONNECT_WAYPOINT_ON_GROUND,
                ktsSpeed: self.trafic_init_position.Airspeed as f64,
                Latitude: self.trafic_init_position.Latitude
                    + 1.2 * (self.trafic_init_position.Heading * DEG_2_RAD).cos(),
                Longitude: self.trafic_init_position.Longitude
                    + 1.2 * (self.trafic_init_position.Heading * DEG_2_RAD).sin()
                        / (self.trafic_init_position.Latitude * DEG_2_RAD).cos(),
                Altitude: self.trafic_init_position.Altitude,
                percentThrottle: 100.0,
            },
        ];

        debug!("set waypoint updated with speed : {} and heading : {}", self.trafic_init_position.Airspeed as i32, self.trafic_init_position.Heading as i32 );
        
        unsafe {
            let res = self.simconnector.set_data_on_sim_object(
                DEF_WP as DWORD,
                object_id,
                SIMCONNECT_DATA_REQUEST_FLAG_DEFAULT,
                0,
                std::mem::size_of_val(&waypoints[0]) as DWORD,
                &waypoints[0] as *const SIMCONNECT_DATA_WAYPOINT as *mut ::std::os::raw::c_void,
            );
            debug!("unsafe called : {}",res);
        };
    }

    fn quit_connexion(&self) {
        let ret = self.simconnector.close();
        info!("simconnect closed : {}", ret.to_string());
    }
}
