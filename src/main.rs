#![allow(dead_code)]
#![allow(unused)]
use std::cell::RefCell;
use std::ops::{Deref, Index};
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::thread::{self, sleep};
use std::time::Duration;
use env_logger::{Builder, Target};
use log::{debug, error, info, warn};

use tceye_rust::domain::conflict::conflict_generator::create_crossing_trafic;
use tceye_rust::domain::scenarios::trafic_params_generator::create_random_trafic_params;
use tceye_rust::domain::simconnectprox::sim_proxy::SimProxy;
use tceye_rust::domain::simconnectprox::simconnect_proxy::SimConnectProxy;
use tceye_rust::domain::traffic::trafic_plane::TraficPlane;

slint::include_modules!();

fn main() -> Result<(), slint::PlatformError> {

    Builder::new().filter_level(log::LevelFilter::Debug).init();

    let ui = AppWindow::new()?;
    info!("app started");

    let simprox = SimConnectProxy::new();

    let simprox_ref = Arc::new(Mutex::new(simprox));
    let simprox_ref_clone = Arc::clone(&simprox_ref);
    let simprox_ref_clone2 = Arc::clone(&simprox_ref);
    let simprox_ref_clone3 = Arc::clone(&simprox_ref);
    let simprox_ref_clone4 = Arc::clone(&simprox_ref);

    const FREQ: u16 = 4;
    const INTER: u16 = 1000 / 16 / FREQ;

    const GLIDER_TRAFIC: TraficPlane = TraficPlane {
        cruise_speed: 60,
        glide_ratio: 48,
    };

    let ui_handle = ui.as_weak();

    //let ui_mutex = Arc::new(Mutex::new(ui_handle));

    ui.on_stop_simco(move || {
        simprox_ref_clone.lock().unwrap().quit_connexion();
    });

    ui.on_request_init_simco(move || {
        simprox_ref.lock().unwrap().init_sim_events();
    });

    ui.on_get_values(move || {
        let sm = simprox_ref_clone2.clone();
        let ui_handle_bis = ui_handle.clone();
        thread::spawn(move || {
            let mut i = 1;
            loop {
                if (i >= INTER) {
                    let res = sm.lock().unwrap().read_sim_values();
                    //let local_ui_handle = ui_handle_bis.clone();
                    //slint::invoke_from_event_loop(move || {
                    //    local_ui_handle.unwrap().set_altitude(res)
                    //});
                    i = 1;
                } else {
                    sm.lock().unwrap().read_sim_values();
                    i += 1;
                }

                sleep(Duration::from_millis(14)); // Will use up lots of CPU if this is not included, as get_next_message() is non-blocking
            }
        });
    });

    ui.on_create_trafic(move || {
        let mut simprox = simprox_ref_clone3.lock().unwrap();
        let last_pos = simprox.get_last_pos_of_player();
        
        let pos_struct = create_crossing_trafic(&last_pos, 25, GLIDER_TRAFIC, 1.852);
        
        simprox.call_ai_aircraft_creation(pos_struct);
    });

    ui.on_start_scenario(move || {
        let sm: Arc<Mutex<SimConnectProxy>> = simprox_ref_clone4.clone();
        thread::spawn(move || {
            loop {
                let trafic_params = create_random_trafic_params();

                let mut simprox = sm.lock().unwrap();
                let last_pos = simprox.get_last_pos_of_player();

                let pos_struct = create_crossing_trafic(
                    &last_pos,
                    trafic_params.time_sep,
                    trafic_params.trafic,
                    trafic_params.bearing as f64,
                );

                simprox.call_ai_aircraft_creation(pos_struct);

                sleep(Duration::from_millis(10000)); // Will use up lots of CPU if this is not included, as get_next_message() is non-blocking
            }
        });
    });

    ui.run()
}

#[cfg(test)]
#[ctor::ctor]
fn init() {
    //Builder::new().filter_level(log::LevelFilter::Info).init();
}
