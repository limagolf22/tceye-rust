#![allow(dead_code)]
#![allow(unused)]
use cucumber::{given, then, when, World};
use tceye_rust::domain::{
    conflict::conflict_generator::create_crossing_trafic,
    simconnectprox::simconnect_params::POS_struct, traffic::trafic_plane::TraficPlane,
};

// `World` is your shared, likely mutable state.
// Cucumber constructs it via `Default::default()` for each scenario.
#[derive(Debug, Default, World)]
pub struct FeatureWorld {
    trafic: POS_struct,
}

// Steps are defined with `given`, `when` and `then` attributes.
#[given("something")]
fn something(world: &mut FeatureWorld) {
    //  println!("given")
}

#[when("a trafic is created")]
fn doit(world: &mut FeatureWorld) {
    const GLIDER_TRAFIC: TraficPlane = TraficPlane {
        cruise_speed: 60,
        glide_ratio: 48,
    };

    let last_pos = POS_struct {
        kohlsman: 29.92,
        altitude: 2000.0,
        latitude: 3.14152 / 4.0,
        longitude: 0.0,
        roll: 0.0,
        pitch: 0.0,
        heading: 0.0,
        speed: 60.0,
    };
    let pos_struct = create_crossing_trafic(&last_pos, 25, GLIDER_TRAFIC, 1.852);

    // println!("when")
    world.trafic = pos_struct;
}

#[then("consequence")]
fn conseq(world: &mut FeatureWorld) {
    //println!("then")
    assert_eq!(0, 0)
}

fn main() {
    futures::executor::block_on(FeatureWorld::run("tests/features"))
}
